#!/bin/sh
echo "\e[32m\e[1mSUCCESS TO SET UP BASE PROJECT....!\e[0m"
echo "
.
├── public
├── src
|   ├── controllers
|   ├── routes
|   ├── views
|   └── index.js
|
├── .git (dir git init)
├── package.json
├── yarn.lock/pakage-lock.json
├── .eslintrc.json
├── .babelrc
├── .env
├── .env.example
├── .gitignore
└── .sequelizerc

"
echo "You can see the file structure above.
Press ctrl + h to see hidden file"
echo "Don't forget to check every directory and file"
echo "Thank You!"
