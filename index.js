import express from 'express'
import logger from 'morgan'
import path from 'path'

const app = express()
const PORT = process.env.PORT || 3100

// base middleware
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, '../public')))

// routes
app.get('/', (req, res) => {
  res.status(200).send('<h1>Hello World!</h1>')
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).send('<h1>404</h1>')
})

app.listen(PORT, () =>
  console.log(`Server running on http://localhost:${PORT}`),
)
