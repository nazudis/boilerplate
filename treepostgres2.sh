#!/bin/sh
echo "\e[32m\e[1mSUCCESS TO SET UP BASE PROJECT....!\e[0m"
echo "
.
├── public
├── src
|   ├── config
|   |   └── config.js
|   ├── controllers
|   ├── migrations
|   ├── models
|   |   └── index.js
|   ├── routes
|   ├── seeders
|   ├── views
|   └── index.js
|
├── .git (dir git init)
├── package.json
├── yarn.lock/pakage-lock.json
├── .babelrc
├── .eslintrc.json
├── .env
├── .env.example
├── .gitignore
└── .sequelizerc

"
echo "You can see the file structure above.
Press ctrl + h to see hidden file"
echo "Don't forget to check every directory and file"
echo "Thank You!"
