#!/bin/sh
echo "\e[32m\e[1mWelcome....!\e[0m"
echo 'This is will generate a starting template for nodejs express. This proccess will take a seconds or a minutes, depends on your internet connection'


echo '
1. Plain NodeJs Express
2. NodeJS with Postgres
3. NodeJs with MongoDB
'
printf 'What do you want to build?[1/2/3]'
read -r code

if [ "$code" = '1' ]; then
  . ~/boilerplate/plain-node.sh
elif [ "$code" = '2' ]; then
  . ~/boilerplate/pg-node.sh
elif [ "$code" = '3' ]; then
  . ~/boilerplate/mg-node.sh
  fi