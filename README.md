# Boilerplate Code (NodeJs/Express)

## Snippet

---

This is just a very simple Boilerplate Code for Nodejs/Express project.
Build with only use shell script, collecting all command to the file, and its will execute as usual.
We all know express already have express-genrator. But i think i just want to create a boilerplate that i wanted.
This will set up basic nodejs server with three options :

1. Basic NodeJs Express Server without DB dependency
2. With Postgresql as DB (includes sequelize, sequelize-cli)
3. With MongoDb as DB (include mongoose)

Cheerssss 🥂.

---

## Usage

```
git clone https://gitlab.com/nazudis/boilerplate.git
```

After clone the repo, do the command bellow to write an alias, so we can execute the command from any where. Please make sure yore clone in the home path, and dont change the directory name make it default `boilerplate`. Change the `.bashrc` if you're not using bash as a default shell.

```
echo 'alias node-project="~/boilerplate/create.sh"' >> ~/.bashrc
```

If you done with the couple of command above, you're ready to go. Just type :

```
node-project
```

That's will ask you for some questions.

---

## Directory Tree and Dependencies

Plain NodeJs Express Project :
dependencies :
`express dotenv morgan`

devdependencies :
`nodemon @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/preset-env babel-eslint eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react`

```
├── public
├── src
|   ├── controllers
|   ├── models
|   ├── routes
|   ├── views
|   └── index.js
|
├── .git (dir git init)
├── package.json
├── yarn.lock/pakage-lock.json
├── .eslintrc.json
├── .babelrc
├── .env
├── .env.example
└── .gitignore
```

NodeJs Express, Postgres as DB :
dependencies :
`express pg sequelize dotenv morgan`

devdependencies :
`nodemon @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/preset-env babel-eslint eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react`

1. Without sequelize init

```
├── public
├── src
|   ├── controllers
|   ├── routes
|   ├── views
|   └── index.js
|
├── .git (dir git init if u initialize)
├── package.json
├── yarn.lock/pakage-lock.json
├── .eslintrc.json
├── .babelrc
├── .env
├── .env.example
├── .gitignore
└── .sequelizerc
```

2. With sequelize init

```
├── public
├── src
|   ├── configs
|   |   └── sequelize
|   |       └── index.js
|   ├── controllers
|   ├── migrations
|   ├── models
|   |   └── index.js
|   ├── routes
|   ├── seeders
|   ├── views
|   └── index.js
|
├── .git (dir git init if u initialize)
├── package.json
├── yarn.lock/pakage-lock.json
├── .babelrc
├── .eslintrc.json
├── .env
├── .env.example
├── .gitignore
└── .sequelizerc
```

NodeJS Express with MongoDB as DB :
dependencies :
`express mongoose dotenv morgan`

devdependencies :
`nodemon @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/preset-env babel-eslint eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react`

```
├── public
├── src
|   ├── controllers
|   ├── models
|   ├── routes
|   ├── views
|   └── index.js
|
├── .git (dir git init if u initialize)
├── package.json
├── yarn.lock/pakage-lock.json
├── .eslintrc.json
├── .babelrc
├── .env
├── .env.example
├── .gitignore
```
