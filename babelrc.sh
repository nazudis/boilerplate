#!/bin/sh
babelrc=.babelrc
echo '{
  "presets": [
    "@babel/preset-env"
  ],
  "plugins": [
    "@babel/transform-runtime",
    "@babel/plugin-proposal-class-properties"
  ],
  "sourceMaps": true,
}' > $babelrc
