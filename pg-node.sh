#!/bin/sh
# Create Project Directory and change to directory
printf 'Directory name : '
read -r dir_name
mkdir "$dir_name"
cd "$dir_name"

# base dir and file
. ~/boilerplate/structurefile.sh

# Create .babelrc, .eslintrc.json
. ~/boilerplate/babelrc.sh
. ~/boilerplate/eslintrc.sh
. ~/boilerplate/sequelizerc.sh

# Initial git
. ~/boilerplate/initialgit.sh

# Install pakage dependency
echo '
1. NPM
2. YARN
'
printf 'Choose the pakage manager : '
read -r pkg_m
if [ "$pkg_m" = '1' ]; then
  unset pkg_m
  pkg_m='npm'
  $pkg_m add express pg sequelize dotenv morgan
  $pkg_m add nodemon sequelize-cli @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/preset-env babel-eslint eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react --save-dev
elif [ "$pkg_m" = '2' ]; then
  unset pkg_m
  pkg_m='yarn'
  $pkg_m add express pg sequelize dotenv morgan
  $pkg_m add nodemon sequelize-cli @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/preset-env babel-eslint eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react --dev
fi

# Set up base server
cp ~/boilerplate/index.js src

printf 'Initial sequelize?[y/n]'
read init_sequelize
if [ "$init_sequelize" = 'y' ] || [ "$init_sequelize" = 'Y' ]; then
	yarn sequelize init
	#DONE
	. ~/boilerplate/treepostgres2.sh
else
	. ~/boilerplate/treepostgres.sh
fi
